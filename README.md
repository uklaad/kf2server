#Killinuxfloor

sudo yum install -y git mc qemu-guest-agent nano

git clone https://github.com/noobient/killinuxfloor.git

cd killinuxfloor

sudo ./install.sh
------------------------

klf stop && klf update && klf config && klf start

------------------------

?Difficulty=3?GameLength=1?Mutator=DamageDisplay.DmgMut,UnofficialKFPatch.UKFPMutator?MaxPlayers=20?LoadFHUD=1?LoadYAS=1?LoadAAL=1?BroadcastPickups=1?CurrentWeekly=19?MaxMonsters=360

Game Difficulty: 0 = Normal, 1 = Hard, 2 = Suicidal, 3 = Hell on Earth Game

Length: 0 = Short, 1 = Medium, 2 = Long

**Bots:**
?Mutator=KF2Bots.KF2BotsMut


*  Mutate Help - Show list of bot commands.
*  Mutate SetNumBots <count> - Change number of bots that will spawn (on server it will be reduced by amount of human players).
*  Mutate SetMinBots <count> - For servers, minimum amount of bots that should always be present.
*  Mutate SetBotMinLv <lv> - Change bots perk minimum level.
*  Mutate SetBotMaxLv <lv> - Change bots perk maximum level.
*  Mutate SetBotChat <true/false> - Enable or disable bots ability for random chat.
*  Mutate SetBotChatWMin <count> - Set bots minimum word vocabulary before they can start to chat.
*  Mutate SetBotChatWMax <count> - Set bots maximum word vocabulary count they can keep track of.

------------------------
* [Port forward](https://wiki.killingfloor2.com/index.php?title=Dedicated_Server_(Killing_Floor_2)&rdfrom=https%3A%2F%2Fwiki.tripwireinteractive.com%2Findex.php%3Ftitle%3DDedicated_Server_%28Killing_Floor_2%29%26redirect%3Dno#Ports)

open IP

------------------------
For step-by-step guides please refer to:

* https://noobient.com/2019/01/11/killinuxfloor-killing-floor-2-server-on-linux/
